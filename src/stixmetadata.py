from typing import List
from pathlib import Path

from mlaslib.models.datamodel import DataModel
from mlaslib.objects.soillayer import SoilLayer

class StixMetaData(DataModel):
    levee_code: str
    levee_chainage: int
    filename: str
    lat: float
    lon: float

    @property 
    def name(self):
        return "_".join(Path(self.filename).stem.split('_')[:2])
