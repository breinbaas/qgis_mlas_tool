import math
from pydantic import BaseModel
from typing import List, Tuple
from mlaslib.objects.points import Point2Dxyc
import numpy as np

class Route(BaseModel):
    name: str = ""
    code: str = ""
    points: List[Point2Dxyc] = []  

    @classmethod
    def from_points(self, code: str, name: str, points: List[Tuple[float, float]]):
        result = Route(
            code = code,
            name = name            
        )

        c = 0
        for i in range(1, len(points)):
            p1 = points[i-1]
            p2 = points[i]
            if i==1:
                result.points.append(Point2Dxyc(x=round(p1[0],2), y=round(p1[1],2), c=0))
            dx = p2[0] - p1[0]
            dy = p2[1] - p1[1]
            c += math.hypot(dx, dy)
            result.points.append(Point2Dxyc(x=round(p2[0],2), y=round(p2[1],2), c=round(c,2)))

        return result

    def regular_spaced_points(self, interval: int=5):
        chs = np.arange(self.min_chainage, self.max_chainage, interval)
        result = []
        for ch in chs:
            x,y,a = self.xya_at_m(ch)
            result.append((ch, x, y))
        return result

    @property
    def max_chainage(self):
        return self.points[-1].c

    @property
    def min_chainage(self):
        return self.points[0].c

    def to_linestring(self):
        result = "LINESTRING("
        for p in self.points:
            result += f"{p.x} {p.y},"
        result = result[:-1] + ")"
        return result

    def xya_at_m(self, m):
        for i in range(1, len(self.points)):
            m1, x1, y1 = self.points[i-1].c, self.points[i-1].x, self.points[i-1].y
            m2, x2, y2 = self.points[i].c, self.points[i].x, self.points[i].y

            if(m1 <= m <= m2):
                x = x1 + (m - m1) / (m2 - m1) * (x2 - x1)
                y = y1 + (m - m1) / (m2 - m1) * (y2 - y1)
                alpha = math.atan2((y1-y2), (x1-x2))
                return x, y, alpha
                
        return 0., 0., 0.

    def get_bounding_box(self, margin=0.):
        result = [1e9,1e9,-1e9,-1e9] #[xmin, ymin, xmax, ymax]
        for p in self.points:
            if p.x < result[0]: result[0] = p.x
            if p.x > result[2]: result[2] = p.x
            if p.y < result[1]: result[1] = p.y
            if p.y > result[3]: result[3] = p.y
        return [result[0]-margin, result[1]-margin, result[2]+margin, result[3]+margin]

    
