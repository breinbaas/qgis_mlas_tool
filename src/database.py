import psycopg2
import pyproj

from .secrets import DB_NAME, DB_USER, DB_PASSWORD, DB_HOST 
from .cptmetadata import CPTMetaData
from mlaslib.objects.crosssection import Crosssection
from mlaslib.creators.geoprofilecreator import GeoprofileCreator

class Database:
    def __init__(self):
        self.conn = psycopg2.connect(f"dbname={DB_NAME} user={DB_USER} password={DB_PASSWORD} host={DB_HOST} port=5432")

    def _read(self, sql):
        cur = self.conn.cursor()
        cur.execute(sql)
        result = [row for row in cur.fetchall()]
        cur.close()
        return result

    def _delete(self, sql):
        cur = self.conn.cursor()
        cur.execute(sql)
        cur.close()
        self.conn.commit()

    def _write(self, sql):
        cur = self.conn.cursor()
        cur.execute(sql)
        cur.close()
        self.conn.commit()

    def get_crosssection_levee_codes(self):
        return self._read("SELECT DISTINCT levee_code FROM crosssections")

    def get_crosssections(self, levee_code, cmin=-1, cmax=-1):
        if cmax==-1 or cmin==-1:
            sql = f"SELECT json FROM crosssections WHERE levee_code='{levee_code}'"
        else:
            sql = f"SELECT json FROM crosssections WHERE levee_code='{levee_code}' AND chainage>={cmin} AND chainage<={cmax}"
        rows = self._read(sql)
        return [Crosssection.from_json(r[0]) for r in rows]

    def remove_crosssection(self, crosssection):
        sql = f"DELETE FROM crosssections WHERE levee_code='{crosssection.levee_code}' and chainage={crosssection.levee_chainage}"
        self._delete(sql)

    def get_cpts_metadata_in_bounding_box(self, bbox):        
        """Returns [(id, name, z)] from the Cpt table if they are in the bounding box"""
        result = []
        xmin, ymin, xmax, ymax = bbox
        proj = pyproj.Transformer.from_crs(4326, 28992, always_xy=True)
        sql = f"SELECT id, name, ST_X(geom), ST_Y(geom), z FROM cpts WHERE geom && ST_MakeEnvelope({xmin}, {ymin}, {xmax}, {ymax}, 4326)"

        for r in self._read(sql):
            id, name, lon, lat, z = r
            x, y = proj.transform(lon, lat)
            result.append(CPTMetaData(id=id, name=name, x=x, y=y, z=z))

        return result

    def get_cpt_data(self, id):
        sql = f"SELECT raw FROM cpts WHERE id={id}"
        return self._read(sql)[0][0]

    def update_crosssection(self, crosssection):
        sql = f"UPDATE crosssections SET json = '{crosssection.to_json()}' WHERE levee_code='{crosssection.levee_code}' and chainage={crosssection.levee_chainage}"
        self._write(sql)


