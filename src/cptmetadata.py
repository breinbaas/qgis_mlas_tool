from typing import List

from mlaslib.models.datamodel import DataModel
from mlaslib.objects.soillayer import SoilLayer

class CPTMetaData(DataModel):
    x: float
    y: float
    id: int
    name: str
    z: float
    soillayers: List[SoilLayer] = []
