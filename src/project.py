import json
from typing import List
from pathlib import Path

from .cptmetadata import CPTMetaData
from .stixmetadata import StixMetaData

from mlaslib.objects.cpt import LlCpt, CPTConversionMethod, CPTInterpretation
from mlaslib.creators.geoprofilecreator import GeoprofileCreator
from mlaslib.objects.soilcollection import SoilCollection
from mlaslib.objects.geoprofile2d import GeoProfile2D
from mlaslib.objects.crosssection import Crosssection
from mlaslib.objects.levee import LeveeSection




class Project:
    def __init__(self):
        self.levee_code = ""
        self.crosssections = []
        self.cpt_metatdata_crest = [] # List[CPTMetaData]
        self.cpt_metatdata_polder = [] # List[CPTMetaData]
        self.geoprofile = None
        self.sections = []
        self.soilcollection = SoilCollection()
        self.is_dirty = False
        self.stix_files: List[StixMetaData] = []

    def clear(self):
        self.levee_code = ""
        self.cpt_metatdata_crest = [] 
        self.cpt_metatdata_polder = []
        self.geoprofile = None
        self.crosssections = []
        self.sections = []
        self.soilcollection = SoilCollection()
        self.is_dirty = False
        self.stix_files = []

    def add_section(self, from_chainage, to_chainage, waterlevel, waterlevel_polder, sfmin, trafficload):
        self.sections.append(
            LeveeSection(
                chainage_start = from_chainage,
                chainage_end = to_chainage,
                waterlevel = waterlevel,
                waterlevel_polder = waterlevel_polder,
                sfmin = sfmin,
                traffic_load = trafficload  
            )
        )
    
    def save(self, filename):
        args = filename.split('.')
        if len(args)>0:
            if args[-1] != 'json':
                filename = f"{filename}.json"

        if self.geoprofile is not None:
            gpjson = self.geoprofile.dict()
        else:
            gpjson = ""

        data = {
            "levee_code":self.levee_code,
            "cpt_metatdata_crest":[cptm.dict() for cptm in self.cpt_metatdata_crest],
            "cpt_metatdata_polder":[cptm.dict() for cptm in self.cpt_metatdata_polder],
            "crosssections": [crs.dict() for crs in self.crosssections],
            "sections": [section.dict() for section in self.sections],
            "geoprofile": gpjson,
            "soilcollection": self.soilcollection.dict(),
            "stix_files": [stixm.dict() for stixm in self.stix_files],
        }

        with open(filename, 'w') as outf:
            json.dump(data, outf)

        self.is_dirty = False

    
    def load(self, filename):
        self.clear()
        with open(filename) as json_file:
            data = json.load(json_file)
            self.levee_code = data['levee_code']
            self.cpt_metatdata_crest = [CPTMetaData.from_json(json.dumps(s)) for s in data['cpt_metatdata_crest']]
            self.cpt_metatdata_polder = [CPTMetaData.from_json(json.dumps(s)) for s in data['cpt_metatdata_polder']]
            self.crosssections = [Crosssection.from_json(json.dumps(s)) for s in data['crosssections']]
            self.sections = [LeveeSection.from_json(json.dumps(s)) for s in data['sections']]
            if data['geoprofile'] != "":
                self.geoprofile = GeoProfile2D.from_json(json.dumps(data['geoprofile']))
            self.soilcollection = SoilCollection.from_json(json.dumps(data['soilcollection']))
            self.stix_files = [StixMetaData.from_json(json.dump(s)) for s in data['stix_files']]

        self._check_stix_files()
        
    def _check_stix_files(self):
        checked_stixms = []
        for stixm in self.stix_files:            
            if Path(stixm.filename).exists():
                checked_stixms.append(stixm)
        self.stix_files = checked_stixms   

    def add_stix_file(self, filename: str):
        fname = Path(filename).stem
        args = fname.split('_')
        if args[0] == self.levee_code:
            try:
                levee_code = args[0]
                levee_chainage = int(args[1])
                if len(args) > 2:
                    lat = float(args[2])
                    lon = float(args[3])
                else:
                    lat, lon = self._resolve_code_chainage_to_lat_lon(levee_code, levee_chainage)

                self.stix_files.append(StixMetaData(        
                    levee_code = levee_code,
                    levee_chainage = levee_chainage,
                    filename = filename,
                    lat = lat,
                    lon = lon,
                ))
            except Exception as e:
                print(f"Error reading stix file properties for file '{filename}', error; '{e}'")

    def get_crosssections(self, cmin, cmax):
        return [crs for crs in self.crosssections if crs.levee_chainage >= cmin and crs.levee_chainage <= cmax]

    def remove_crosssection(self, crs):
        self.crosssections.remove(crs)
        self.is_dirty = True

    def get_section_at_chainage(self, chainage):
        for section in self.sections:
            if section.chainage_start <= chainage and chainage <= section.chainage_end:
                return section

        return None

    def cpt_metadata_from_name(self, name):
        for cptm in self.cpt_metatdata_polder + self.cpt_metatdata_crest:
            if cptm.name == name:
                return cptm
        return None
    
    def l_transition_from_l(self, cmin, cmax, l):
        for crs in self.crosssections:
            if cmin <= crs.levee_chainage and crs.levee_chainage <= cmax:
                crs.add_fixed_transition(l)
        self.is_dirty = True
       
    def move_cpts_to_polder(self, ids):
        self.cpt_metatdata_polder += [cpt for cpt in self.cpt_metatdata_crest if cpt.name in ids]
        self.cpt_metatdata_polder = sorted(self.cpt_metatdata_polder, key=lambda x:x.name)
        self.cpt_metatdata_crest = sorted([cpt for cpt in self.cpt_metatdata_crest if not cpt.name in ids], key=lambda x:x.name)
        self.is_dirty = True

    def move_cpts_to_crest(self, ids):
        self.cpt_metatdata_crest += [cpt for cpt in self.cpt_metatdata_polder if cpt.name in ids]
        self.cpt_metatdata_crest = sorted(self.cpt_metatdata_crest, key=lambda x:x.name)
        self.cpt_metatdata_polder = sorted([cpt for cpt in self.cpt_metatdata_polder if not cpt.name in ids], key=lambda x:x.name)
        self.is_dirty = True

    def remove_crest_cpts(self, ids):
        self.cpt_metatdata_crest = [cpt for cpt in self.cpt_metatdata_crest if not cpt.name in ids]       
        self.is_dirty = True 

    def remove_polder_cpts(self, ids):
        self.cpt_metatdata_polder = [cpt for cpt in self.cpt_metatdata_polder if not cpt.name in ids]  
        self.is_dirty = True

    def generate_geoprofile(self, database, refline_points, max_cpt_distance, max_borehole_distance, min_layer_height, cpt_conversion_method_text, friction_ratio_peat):
        log = []
        cpts_crest, cpts_polder, cpt_interpretations = [], [], []
        for cptm in self.cpt_metatdata_crest:
            try:
                cpt_data = database.get_cpt_data(cptm.id)
                cpts_crest.append(LlCpt.from_string(cpt_data))
                if len(cptm.soillayers) > 0:
                    cpt_interpretations.append(CPTInterpretation(cpt_name=cptm.name, soillayers=cptm.soillayers))    
            except Exception as e:
                log.append(f"Error reading crest cpt '{cptm.id}'; {e}")

        for cptm in self.cpt_metatdata_polder:
            try:
                cpt_data = database.get_cpt_data(cptm.id)
                cpts_polder.append(LlCpt.from_string(cpt_data)) 
                if len(cptm.soillayers) > 0:
                    cpt_interpretations.append(CPTInterpretation(cpt_name=cptm.name, soillayers=cptm.soillayers))         

            except Exception as e:
                log.append(f"Error reading polder cpt '{cptm.id}'; {e}")

        cpt_conversion_method = CPTConversionMethod.ROBERTSON
        if cpt_conversion_method_text == 'robertson':
            cpt_conversion_method = CPTConversionMethod.ROBERTSON
        try:
            gpc = GeoprofileCreator(
                cpts_crest = cpts_crest,
                cpts_polder=cpts_polder,
                refline_points=refline_points,
                cpt_conversion_method=cpt_conversion_method,
                minimum_layerheight=min_layer_height,
                maximum_distance=max_cpt_distance,
                cpt_interpretations=cpt_interpretations,
                friction_ratio_peat=friction_ratio_peat,                
            )
            self.geoprofile = gpc.execute()            
        except Exception as e:
            log.append(f"Error creating geotechnical profile, {e}")
            print(log)
            return

        for l in gpc.log:
            log.append(l)
        
        self.is_dirty = True
        return log